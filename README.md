# uleddev
Tool to create new userspace LED class devices and send leds states to an Arduino(-like) board to drive real leds.

Linux Kernel 4.10 or above required, with uleds fonctionnality (`CONFIG_LEDS_USER`) by David Lechner. See [Kernel Documentation](https://github.com/torvalds/linux/blob/master/Documentation/leds/uleds.txt) for more information.

Build with `make`, then use `uleddev` to create one or more `/sys/class/leds/` directories, named from `-d` and `-n` options.

Usage: `uledmon -d <device-name> -n <num>`

- `<device-name>` is the basename of the LEDs class device to be created (default "uled:").
- `<num>` set the number of LEDs to create (default 1).
- `-p <file>` is the path to serial connexion to Arduino board (default `/dev/ttyUSB0`).
- `-s` simulate (create `/sys/class/leds/` entries but don't send anything.
- `-v` be verbose (can be used multiple time to increase verbosity).
- `-D` run in daemon mode (fork to the background)

`Arduino_APA102` and `Arduino_ledsIO` are Arduino sketchs that listen to serial messages and change leds state (APA102 or simple GPIO leds). In theory, the design can manage up to 127 leds. Each message is one byte long, 7 bits for led number and one bit (LSB) for led state. Actually driving the leds is the job of the Arduino sketch.

Example Usage :

```
$ ls -l /sys/class/leds/
total 0
lrwxrwxrwx 1 root root 0 juin   1 12:52 input0::capslock -> ../../devices/platform/i8042/serio0/input/input0/input0::capslock
lrwxrwxrwx 1 root root 0 juin   1 12:52 input0::numlock -> ../../devices/platform/i8042/serio0/input/input0/input0::numlock
lrwxrwxrwx 1 root root 0 juin   1 12:52 input0::scrolllock -> ../../devices/platform/i8042/serio0/input/input0/input0::scrolllock
lrwxrwxrwx 1 root root 0 juin   1 12:52 mmc0:: -> ../../devices/pci0000:00/0000:00:1d.3/0000:06:00.0/leds/mmc0::
lrwxrwxrwx 1 root root 0 juin   1 12:52 phy0-led -> ../../devices/pci0000:00/0000:00:1d.2/0000:05:00.0/leds/phy0-led
lrwxrwxrwx 1 root root 0 juin   1 12:52 tpacpi::kbd_backlight -> ../../devices/platform/thinkpad_acpi/leds/tpacpi::kbd_backlight
lrwxrwxrwx 1 root root 0 juin   1 12:52 tpacpi::power -> ../../devices/platform/thinkpad_acpi/leds/tpacpi::power
lrwxrwxrwx 1 root root 0 juin   1 12:52 tpacpi::standby -> ../../devices/platform/thinkpad_acpi/leds/tpacpi::standby
lrwxrwxrwx 1 root root 0 juin   1 12:52 tpacpi::thinklight -> ../../devices/platform/thinkpad_acpi/leds/tpacpi::thinklight
lrwxrwxrwx 1 root root 0 juin   1 12:52 tpacpi::thinkvantage -> ../../devices/platform/thinkpad_acpi/leds/tpacpi::thinkvantage

# create HiThere_0 to HiThere_5
$ sudo ./uleddev -d HiThere_ -n 5

# leds added to /sys/class/leds/
$ ls -l /sys/class/leds/
total 0
lrwxrwxrwx 1 root root 0 juin   4 10:09 HiThere_0 -> ../../devices/virtual/misc/uleds/HiThere_0
lrwxrwxrwx 1 root root 0 juin   4 10:09 HiThere_1 -> ../../devices/virtual/misc/uleds/HiThere_1
lrwxrwxrwx 1 root root 0 juin   4 10:09 HiThere_2 -> ../../devices/virtual/misc/uleds/HiThere_2
lrwxrwxrwx 1 root root 0 juin   4 10:09 HiThere_3 -> ../../devices/virtual/misc/uleds/HiThere_3
lrwxrwxrwx 1 root root 0 juin   4 10:09 HiThere_4 -> ../../devices/virtual/misc/uleds/HiThere_4
lrwxrwxrwx 1 root root 0 juin   1 12:52 input0::capslock -> ../../devices/platform/i8042/serio0/input/input0/input0::capslock
lrwxrwxrwx 1 root root 0 juin   1 12:52 input0::numlock -> ../../devices/platform/i8042/serio0/input/input0/input0::numlock
lrwxrwxrwx 1 root root 0 juin   1 12:52 input0::scrolllock -> ../../devices/platform/i8042/serio0/input/input0/input0::scrolllock
lrwxrwxrwx 1 root root 0 juin   1 12:52 mmc0:: -> ../../devices/pci0000:00/0000:00:1d.3/0000:06:00.0/leds/mmc0::
lrwxrwxrwx 1 root root 0 juin   1 12:52 phy0-led -> ../../devices/pci0000:00/0000:00:1d.2/0000:05:00.0/leds/phy0-led
lrwxrwxrwx 1 root root 0 juin   1 12:52 tpacpi::kbd_backlight -> ../../devices/platform/thinkpad_acpi/leds/tpacpi::kbd_backlight
lrwxrwxrwx 1 root root 0 juin   1 12:52 tpacpi::power -> ../../devices/platform/thinkpad_acpi/leds/tpacpi::power
lrwxrwxrwx 1 root root 0 juin   1 12:52 tpacpi::standby -> ../../devices/platform/thinkpad_acpi/leds/tpacpi::standby
lrwxrwxrwx 1 root root 0 juin   1 12:52 tpacpi::thinklight -> ../../devices/platform/thinkpad_acpi/leds/tpacpi::thinklight
lrwxrwxrwx 1 root root 0 juin   1 12:52 tpacpi::thinkvantage -> ../../devices/platform/thinkpad_acpi/leds/tpacpi::thinkvantage

# Like any other leds, you get brightness control and trigger
$ ls -l /sys/class/leds/HiThere_2/
total 0
-rw-r--r-- 1 root root 4096 juin   4 10:10 brightness
lrwxrwxrwx 1 root root    0 juin   4 10:10 device -> ../../uleds
-r--r--r-- 1 root root 4096 juin   4 10:10 max_brightness
drwxr-xr-x 2 root root    0 juin   4 10:10 power
lrwxrwxrwx 1 root root    0 juin   4 10:10 subsystem -> ../../../../../class/leds
-rw-r--r-- 1 root root 4096 juin   4 10:10 trigger
-rw-r--r-- 1 root root 4096 juin   4 10:09 uevent

$ cat /sys/class/leds/HiThere_2/trigger
[none] kbd-scrolllock kbd-numlock kbd-capslock kbd-kanalock kbd-shiftlock
kbd-altgrlock kbd-ctrllock kbd-altlock kbd-shiftllock kbd-shiftrlock
kbd-ctrlllock kbd-ctrlrlock disk-activity ide-disk mtd nand-disk cpu cpu0
cpu1 cpu2 cpu3 cpu4 cpu5 cpu6 cpu7 panic usb-gadget usb-host mmc0 rfkill-any
rfkill1 phy0rx phy0tx phy0assoc phy0radio BAT0-charging-or-full BAT0-charging
BAT0-full BAT0-charging-blink-full-solid AC-online rfkill2 heartbeat rfkill4

# Using heartbeat trigger here
$ sudo sh -c 'echo heartbeat > /sys/class/leds/HiThere_2/trigger'

# And uleddev make Arduino board bling led 2...
```
