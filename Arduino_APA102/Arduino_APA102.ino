#include <FastLED.h>

#define NBRPIX     10
#define DATA_PIN   11
#define CLOCK_PIN  13

CRGBArray<NBRPIX> leds;

void setup() {
  FastLED.addLeds<APA102, DATA_PIN, CLOCK_PIN, BGR>(leds, NBRPIX).setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(15);

  Serial.begin(115200);

  FastLED.clear();
  FastLED.show();

  for(int i = 0; i < NBRPIX; i++) {
    leds[i] = CRGB::Red;
    FastLED.show();
    FastLED.delay(20);
  }

  FastLED.delay(150);
  for(int i = 0; i < NBRPIX; i++) {
    leds[i] = CRGB::Black;
    FastLED.show();
    FastLED.delay(20);
  }
}

void loop() {
  if (Serial.available() > 0) {
    unsigned char val = Serial.read();
    if(val & 0x01) {
      leds[(val >> 1)] = CRGB::Red;
    } else {
      leds[(val >> 1)] = CRGB::Black;
    }
    FastLED.show();
  }
}
