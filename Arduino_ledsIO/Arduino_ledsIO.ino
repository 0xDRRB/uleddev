#define NBRPIX  10
unsigned int leds[10] = {11,10,9,8,7,6,5,4,3,2};

void setup() {

  for(int i = 0; i < 10; i++) {
    pinMode(leds[i], OUTPUT);
  }
  
  Serial.begin(115200);


  // test
  for(int i = 0; i < NBRPIX; i++) {
    digitalWrite(leds[i], HIGH);
    delay(20);
  }
  delay(150);
  for(int i = 0; i < NBRPIX; i++) {
    digitalWrite(leds[i], LOW);
    delay(20);
  }
}

void loop() {
  if (Serial.available() > 0) {
    unsigned char val = Serial.read();
    if(val & 0x01) {
      digitalWrite(leds[(val >> 1)], HIGH);
    } else {
      digitalWrite(leds[(val >> 1)], LOW);
    }
  }
}
