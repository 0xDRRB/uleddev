/*
 * uleddev.c
 * This program creates a new userspace LED class device and monitors it. A
 * timestamp and brightness value is printed each time the brightness changes.
 * Usage: uledmon -d <device-name> -n <num>
 * <device-name> is the basename of the LEDs class device to be created.
 * <num> set the number of LEDs to create
 * Pressing CTRL+C will exit.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <signal.h>
#include <poll.h>

#include <sys/errno.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <termios.h>

#include <linux/uleds.h>

#define ULED_MAXDEV     127
#define ULED_BASENAME   "uled:"

#define SERIALDEVICE "/dev/ttyUSB0"
//#define BAUDRATE B230400
#define BAUDRATE B115200

struct pollfd *pfds;
int numdev = 1;
int useserial = 1;
int serfd;
int verbose = 0;
struct termios oldtio, newtio;

void sig_handler(int signo) {
	if(signo == SIGINT || signo == SIGTERM) {
		for(int i = 0; i < numdev; i++) {
			close(pfds[i].fd);
		}

		if(useserial) {
			// set old config
			if (tcsetattr(serfd,TCSANOW,&oldtio) != 0) {
				fprintf(stderr,"tcsetattr Error : %s (%d)\n", strerror(errno),errno);
				exit(EXIT_FAILURE);
			}
			close(serfd);
		}
		if(verbose)
			printf("\nBye.\n");
		exit(EXIT_SUCCESS);
	}
}

void printhelp(char *binname) {
	printf("Userspace LED class device creator v0.0.1\n");
	printf("Copyright (c) 2018 - Denis Bodor\n\n");
	printf("Usage : %s [OPTIONS]\n", binname);
	printf(" -d name             base ledname to create (default 'uled:')\n");
	printf(" -n num              number of leds to create (default 1)\n");
	printf(" -s                  screen only, don't send to serial port (default no, use serial device)\n");
	printf(" -p file             full path to serial dev node (default %s)\n", SERIALDEVICE);
	printf(" -v                  be verbose (multiple time to increase verbosity)\n");
	printf(" -D                  Run daemon in the background\n");
	printf(" -h                  show this help\n");
}

int main(int argc, char** argv)
{
	int retopt;
	int opt = 0;
	char *endptr;
	int daemonize = 0;

	char *basename = NULL;
	struct uleds_user_dev uleds_dev[ULED_MAXDEV] = {0};

	int ret, pret;
	int brightness[ULED_MAXDEV];
	struct timespec ts;

	char *filename = NULL;
	unsigned char data;

	pid_t cpid;

	while ((retopt = getopt(argc, argv, "d:n:p:shvD")) != -1) {
		switch (retopt) {
			case 'd':
				if(strlen(optarg) + 4 > 64) {
					printf("Device name too long (max 60 char)\n");
					return(EXIT_FAILURE);
				}
				basename = strdup(optarg);
				opt++;
				break;
			case 'n':
				numdev = (int)strtol(optarg, &endptr, 10);
				if(endptr == optarg || numdev <= 0 || numdev > ULED_MAXDEV) {
					printf("You must specify a valid number (1-255)\n");
					return(EXIT_FAILURE);
				}
				opt++;
				break;
			case 'p':
				filename = optarg;
				opt++;
				break;
			case 's':
				useserial = 0;
				opt++;
				break;
			case 'h':
				printhelp(argv[0]);
				return(EXIT_SUCCESS);
				opt++;
				break;
			case 'v':
				verbose++;
				opt++;
				break;
			case 'D':
				daemonize = 1;
				opt++;
				break;
			default:
				printhelp(argv[0]);
				return(EXIT_FAILURE);
		}
	}

	/* serial stuff */
	if(useserial) {
		if(!filename)
			filename=strdup(SERIALDEVICE);

		if ((serfd = open(SERIALDEVICE, O_RDWR | O_NOCTTY )) < 0) {
			fprintf(stderr,"Serial Open Error : %s (%d)\n", strerror(errno),errno);
			return(EXIT_FAILURE);
		}

		if(tcgetattr(serfd,&oldtio) != 0) {
			fprintf(stderr,"tcgetattr Error : %s (%d)\n", strerror(errno),errno);
			return(EXIT_FAILURE);
		}

		// control mode flags
		newtio.c_cflag = BAUDRATE | CSTOPB | CREAD | CS8 | CLOCAL;
		// input mode flags
		newtio.c_iflag = IGNBRK;
		// output mode flags
		newtio.c_oflag = 0;
		// local mode flags
		newtio.c_lflag = 0;

		// flush all data
		if(tcflush(serfd, TCIOFLUSH) != 0) {
			fprintf(stderr,"tcflush Error : %s (%d)\n", strerror(errno),errno);
			exit(EXIT_FAILURE);
		}

		// set new config
		if (tcsetattr(serfd,TCSANOW,&newtio) != 0) {
			fprintf(stderr,"tcsetattr Error : %s (%d)\n", strerror(errno),errno);
			exit(EXIT_FAILURE);
		}
	}

	/* /dev/uleds stuff */
	if(basename == NULL)
		basename = strdup(ULED_BASENAME);

	if(verbose) {
		printf("Device basename: '%s' (%lu)\n", basename, strlen(basename));
		printf("Number of devices to create: %u\n", numdev);
	}

	if((pfds = calloc(numdev, sizeof(struct pollfd))) == NULL) {
		fprintf(stderr,"Allocation Error!\n");
		return(EXIT_FAILURE);
	}

	for(int i = 0; i < numdev; i++) {
		// name the led
		strcpy(uleds_dev[i].name, basename);
		sprintf(uleds_dev[i].name+strlen(basename), "%u", i);
		uleds_dev[i].max_brightness = 100;

		// open and create led
		pfds[i].fd = open("/dev/uleds", O_RDWR);
		if(pfds[i].fd == -1) {
			perror("Failed to open /dev/uleds");
			return(EXIT_FAILURE);
		}
		ret = write(pfds[i].fd, uleds_dev+i, sizeof(uleds_dev[0]));
		if(ret == -1) {
			perror("Failed to write to /dev/uleds");
			return(EXIT_FAILURE);
		}

		// we wait for data to read
		pfds[i].events = POLLIN;
	}

	if(signal(SIGINT, sig_handler) == SIG_ERR) {
		fprintf(stderr,"Can't catch SIGINT\n");
		return(EXIT_FAILURE);
	}

	if(signal(SIGTERM, sig_handler) == SIG_ERR) {
		fprintf(stderr,"Can't catch SIGINT\n");
		return(EXIT_FAILURE);
	}

	if(daemonize) {
		cpid = fork();
		if(cpid < 0) {
			fprintf(stderr,"Can't fork !\n");
		}
		// parent must die
		if(cpid > 0) {
			if(verbose)
				printf("Forked ! %u will do the job now.\n", cpid);
			return(EXIT_SUCCESS);
		}

		// Detach all standard I/O descriptors
		close(STDIN_FILENO);
		close(STDOUT_FILENO);
		close(STDERR_FILENO);
		// Enter a new session
		setsid();
	}

	while(1) {
		// poll blocks with infinite timeout
		pret = poll(pfds, numdev, -1);

		if(pret) {
			if(pret == -1) {
				fprintf(stderr,"Poll Error!\n");
				return(EXIT_FAILURE);
			}
			// which fd to read ?
			for(int i = 0; i < numdev; i++) {
				if(pfds[i].revents & POLLIN) {
					ret = read(pfds[i].fd, brightness+i, sizeof(brightness[0]));
					if(ret == -1) {
						perror("Failed to read from /dev/uleds");
						return(EXIT_FAILURE);
					}
					if(verbose > 1) {
						clock_gettime(CLOCK_MONOTONIC, &ts);
						printf("(%u) [%ld.%09ld] %u\n", i, ts.tv_sec, ts.tv_nsec, brightness[i]);
					}
					if(useserial) {
						if(brightness[i]) {
							// led on
							data = (i << 1) | 0x01;
						} else {
							// led off
							data = (i << 1);
						}
						if(write(serfd, &data, sizeof(data)) != sizeof(data)){
							fprintf(stderr,"Write Error : %s (%d)\n", strerror(errno),errno);
						}
					}
				}
			}
		}
	}

	for(int i = 0; i < numdev; i++) {
		close(pfds[i].fd);
	}

	if(useserial) {
		// set old config
		if (tcsetattr(serfd,TCSANOW,&oldtio) != 0) {
			fprintf(stderr,"tcsetattr Error : %s (%d)\n", strerror(errno),errno);
			exit(EXIT_FAILURE);
		}
		close(serfd);
	}

	return(EXIT_SUCCESS);
}
